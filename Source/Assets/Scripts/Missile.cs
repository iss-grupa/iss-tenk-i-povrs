using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    private CameraControllerRocket cameraController;
    public GameObject blastFX;

    private float forwardSpeed = 45f;
    private float strafeSpeed = 5f;
    private float hoverSpeed = 5f;

    private float activeForwardSpeed;
    private float activeStrafeSpeed;
    private float activeHoverSpeed;

    private float forwardAcceleration = 5f;
    private float strafeAcceleration = 0.2f;
    private float hoverAcceleration = 0.2f;

    private float lookRotateSpeed = 45f;

    public AudioSource explosionAudio;
    public AudioClip explosionAudioClip;


    void Awake()
    {
        cameraController = FindObjectOfType<CameraControllerRocket>();
    }


    // Update is called once per frame
    void LateUpdate()
    {
        if (cameraController.missileCameraActive)
        {
            transform.Rotate(Input.GetAxisRaw("Vertical Rocket") * lookRotateSpeed * Time.deltaTime, Input.GetAxisRaw("Horizontal Rocket") * lookRotateSpeed * Time.deltaTime, 0f, Space.Self);
        }


        activeForwardSpeed = Mathf.Lerp(activeForwardSpeed, forwardSpeed, forwardAcceleration * Time.deltaTime);
        activeStrafeSpeed = Mathf.Lerp(activeStrafeSpeed, Input.GetAxisRaw("Horizontal Rocket") * strafeSpeed, strafeAcceleration * Time.deltaTime);
        activeHoverSpeed = Mathf.Lerp(activeHoverSpeed, Input.GetAxisRaw("Vertical Rocket") * hoverSpeed, hoverAcceleration * Time.deltaTime);

        transform.position += transform.forward * activeForwardSpeed * Time.deltaTime;
        transform.position += transform.right * activeStrafeSpeed * Time.deltaTime;
        transform.position += transform.up * activeHoverSpeed * Time.deltaTime;
    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Tank")
        {
            Destroy(collision.gameObject);
        }

        Instantiate(blastFX, transform.position, transform.rotation);
        explosionAudio.Play();
        AudioSource.PlayClipAtPoint(explosionAudioClip, transform.position, 1f);

        Destroy(gameObject);
        cameraController.UseLauncherCamera();
    }
}
