using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour
{
    private CameraControllerRocket cameraController;
    public GameObject missile;
    public AudioSource launchAudio;
    public AudioClip launchAudioClip;

    private float lookRotateSpeed = 40f;

    void Awake()
    {
        cameraController = FindObjectOfType<CameraControllerRocket>();
    }


    void Update()
    {
        //freezanje rotacije na Z osi
        Quaternion q = transform.rotation;
        q.eulerAngles = new Vector3(q.eulerAngles.x, q.eulerAngles.y, 0);
        transform.rotation = q;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (!cameraController.missileCameraActive) 
        {
            //Debug.Log("A");
            transform.Rotate(Input.GetAxisRaw("Vertical Rocket") * lookRotateSpeed * Time.deltaTime, Input.GetAxisRaw("Horizontal Rocket") * lookRotateSpeed * Time.deltaTime, 0f, Space.World);
        }
        

        if (Input.GetKeyDown("space"))
        {
            Quaternion q = transform.rotation;
            q.eulerAngles = new Vector3(-q.eulerAngles.x, q.eulerAngles.y - 180, q.eulerAngles.z);

            Instantiate(missile, this.transform.position, q);
            launchAudio.Play();
            AudioSource.PlayClipAtPoint(launchAudioClip, transform.position, 1f);
            cameraController.UseMissileCamera();
        } 
    }
}
