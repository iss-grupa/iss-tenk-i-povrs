using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    float x, y;
    float xRotation = 0f;
    public float sensitivity = 100f;
    public Transform target;
    public Transform barrel;

    private void Start ()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void LateUpdate(){
        x = Input.GetAxis("Mouse X")*sensitivity*Time.deltaTime;
        y = Input.GetAxis("Mouse Y")*sensitivity*Time.deltaTime;

        xRotation -= y;
        xRotation = Mathf.Clamp(xRotation, 80f, 100f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        barrel.localRotation = Quaternion.Euler(xRotation - 90f, 0f, 0f);
        target.Rotate(Vector3.forward * x);
        
    }
}