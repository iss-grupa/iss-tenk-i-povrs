using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControllerRocket : MonoBehaviour
{
    private Camera launcherCamera;
    private Camera missileCamera;

    public bool missileCameraActive = false;


    void Start()
    {
        UseLauncherCamera();
    }


    public void UseMissileCamera() 
    {
        missileCamera = GameObject.Find("Missile Camera").GetComponent<Camera>();
        missileCamera.enabled = true;
        launcherCamera.enabled = false;

        missileCameraActive = true;
    }

    public void UseLauncherCamera()
    {
        launcherCamera = GameObject.Find("Launcher Camera").GetComponent<Camera>();
        launcherCamera.enabled = true;
        if(missileCamera!= null)
        {
            missileCamera.enabled = false;
        }

        missileCameraActive = false;
    }
}



/* using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;

    public Transform targetPoint;

    public float moveSpeed = 8f, rotateSpeed = 3f;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, targetPoint.position, moveSpeed * Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetPoint.rotation, rotateSpeed * Time.deltaTime);
        
    }
}
 */