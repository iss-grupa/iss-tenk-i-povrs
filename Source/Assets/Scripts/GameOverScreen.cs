using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameOverScreen : MonoBehaviour
{
    public TMP_Text winnerText;


    public void Setup(string winnerObject) {
        gameObject.SetActive(true);
        winnerText.text = winnerObject + " WON!";
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R)) {
            SceneManager.LoadScene("SampleScene");
        }

        if (Input.GetKeyDown(KeyCode.Q)){
            Application.Quit();
        }
    }

    
}
