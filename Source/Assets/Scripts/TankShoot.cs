using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShoot : MonoBehaviour {
   public Rigidbody shell;                     // Prefab of the shell.
   public Transform fireTransform;             // A child of the tank where the shells are spawned.
   public AudioSource shootingAudio;           // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source.
   public AudioClip fireAudio;                 // Audio that plays when each shot is fired.
   public float launchForce = 0.5f;             // The force given to the shell if the fire button is not held.
   public GameObject FlashFx;

   private string fireButton;                  // The input axis that is used for launching shells.
   private bool fired;                       // Whether or not the shell has been launched with this button press.


   private void Start ()
   {
      // The fire axis is based on the player number.
      fireButton = "Fire1";

   }

   private void Update ()
   {
      // Start the coroutine named FiringCoroutine
      StartCoroutine(FiringCoroutine());
   }

   private IEnumerator FiringCoroutine() {
      // If fireButton is pressed and fired is false 
      if (Input.GetButtonDown (fireButton) && !fired)
      {
         // ... launch the shell.
         Fire ();
         // wait for 2 seconds
         yield return new WaitForSeconds (2);
         fired = false;
      }
   }

   private void Fire ()
   {
      // Set the fired flag so only Fire is only called once.
      fired = true;

      // Create an instance of the shell and store a reference to it's rigidbody.
      Rigidbody shellInstance =
         Instantiate (shell, fireTransform.position, fireTransform.rotation) as Rigidbody;

      // Set the shell's velocity to the launch force in the fire position's forward direction.
      shellInstance.velocity = launchForce * fireTransform.forward;

      // Change the clip to the firing clip and play it.
      Instantiate(FlashFx, fireTransform.position, fireTransform.rotation);
      shootingAudio.clip = fireAudio;
      shootingAudio.Play ();
   }
}