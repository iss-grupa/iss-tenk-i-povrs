using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankController : MonoBehaviour
{
    public Transform[] WheelMeshes;
    public WheelCollider[] WheelColls;
    Vector3 pos, rotation;
    Quaternion quat;
    public float Force = 800, RotSpeed = 0.5f;
    private void Start()
    {
        rotation = transform.eulerAngles;
    }

    private void LateUpdate()
    {
        transform.eulerAngles = rotation;
        var transAmount = Mathf.Clamp(RotSpeed * Time.deltaTime, 0f , 1.5f);

        if (Input.GetKey("up")) {
            transform.Translate(0, 0, transAmount);
        }
        if (Input.GetKey("down")) {
            transform.Translate(0, 0, -transAmount);
        }


        /* for (int i = 0; i < WheelColls.Length; i++)
        {
            WheelColls[i].GetWorldPose(out pos, out quat);
            WheelMeshes[i].position = pos;
            WheelMeshes[i].rotation = quat;
        }

        foreach (var wheelcols in WheelColls)
        {
            wheelcols.motorTorque = Input.GetAxis("Vertical Tank") * Force * Time.deltaTime;
        } */

        rotation.y += Mathf.Clamp(Input.GetAxis("Horizontal Tank") * RotSpeed, -0.4f, 0.4f);
    }
}