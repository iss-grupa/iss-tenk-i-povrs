using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellExplosion : MonoBehaviour
{
    public AudioSource explosionAudio;
    public AudioClip explosionAudioClip;
    public float maxLifeTime = 5f;
    public float explosionRadius = 5f;
    //public float explosionForce = 1000f;
    public GameObject blastFX;

    private void Start()
    {
        Destroy(gameObject, maxLifeTime);   //uništavanje objekta nakon 5 sekundi
    }

    /*
    private void OnTriggerEnter(Collider other)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);

        for (int i = 0; i < colliders.Length; i++) 
        {
            Rigidbody targetRigidBody = colliders[i].GetComponent<Rigidbody>();
            
            if (!targetRigidBody)
                continue;
            
            targetRigidBody.AddExplosionForce (explosionForce, transform.position, explosionRadius);

            Damage damage = targetRigidBody.GetComponent<Damage>();

            if (!damage) 
                continue;
            
            damage.DestroyObject();
        }

        Instantiate(blastFX, transform.position, transform.rotation);
        explosionAudio.Play();
        AudioSource.PlayClipAtPoint(explosionAudioClip, transform.position, 1f);
        Destroy(gameObject);
    }*/

    private void OnCollisionEnter(Collision collision)
    {
        Instantiate(blastFX, transform.position, transform.rotation);
        explosionAudio.Play();
        AudioSource.PlayClipAtPoint(explosionAudioClip, transform.position,1f);

        try
        {
            if (collision.transform.parent.tag == "House")
            {
                Destroy(collision.transform.parent.gameObject);
            }
        }
        catch
        {
            
        }
        finally
        {
            Destroy(gameObject);
        }
    }
}
