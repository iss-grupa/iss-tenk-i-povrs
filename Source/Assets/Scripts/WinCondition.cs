using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinCondition : MonoBehaviour
{

    private GameObject tank;
    private GameObject launcher;
    private GameObject terrain;
    public GameOverScreen GameOverScreen;
    private string winnerObject;


    void Awake()
    {
        terrain = GameObject.Find("Terrain");
        terrain.GetComponent<TerrainCollider>().enabled = false;
        terrain.GetComponent<TerrainCollider>().enabled = true;

        tank = GameObject.Find("Tank");
        launcher = GameObject.Find("RPG7");
    }
    // Update is called once per frame
    void LateUpdate()
    {
        if(tank == null)
        {
            winnerObject = "Launcher";
            GameOver();
        }
        else if (launcher == null)
        {
            winnerObject = "Tank";
            GameOver();
        }
    }

    public void GameOver() {
         GameOverScreen.Setup(winnerObject);
    }
}
