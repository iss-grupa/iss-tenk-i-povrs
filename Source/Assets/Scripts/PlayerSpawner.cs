using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    [SerializeField]
    [Tooltip("4 possible spawn locations for the player followed by 4 possible spawn locations for the tank")]
    private Transform position0, position1, position2, position3, position4, position5, position6, position7;

    [SerializeField]
    [Tooltip("Object containing both the player POV and his rocket launcher")]
    private GameObject spawnedPlayer;

    private int randomPosition;
    private GameObject spawnedFlag;
    private GameObject spawnedTank;

    // Start is called before the first frame update
    void Start()
    {
        spawnedTank = GameObject.Find("Tank");
        spawnedFlag = GameObject.Find("flag_med");
        spawnedFlag.transform.rotation = Quaternion.Euler(0, 0, 0);
        SpawnPlayer();
    }

    private void SpawnPlayer()
    {
        randomPosition = UnityEngine.Random.Range(0, 4);
        if (randomPosition == 0)
        {
            spawnedPlayer.transform.position = position0.position;
            spawnedPlayer.transform.rotation = position0.rotation;
            spawnedFlag.transform.position = new Vector3(146.96f, 40.65f, 85.12f);
            spawnedTank.transform.position = position4.position;
        }
        else if (randomPosition == 1) {
            spawnedPlayer.transform.position = position1.position;
            spawnedPlayer.transform.rotation = position1.rotation;
            spawnedFlag.transform.position = new Vector3(27, 2, 436);
            spawnedTank.transform.position = position5.position;
        }
        else if (randomPosition == 2)
        {
            spawnedPlayer.transform.position = position2.position;
            spawnedPlayer.transform.rotation = position2.rotation;
            spawnedFlag.transform.position = new Vector3(450, 59, 63);
            spawnedTank.transform.position = position6.position;
        }
        else
        {
            spawnedPlayer.transform.position = position3.position;
            spawnedPlayer.transform.rotation = position3.rotation;
            spawnedFlag.transform.position = new Vector3(492.41f, 43.2f, 491.25f);
            spawnedTank.transform.position = position7.position;
        }

        ConnectToLauncher();
        
    }

    void ConnectToLauncher()
    {
        RaycastHit hit;
        if (Physics.Raycast(spawnedPlayer.transform.position, transform.up, out hit))
        {
            if (hit.transform.parent.tag == "House")
            {
                spawnedPlayer.transform.SetParent(hit.transform.parent, true); 
            }
        }
    }
}
